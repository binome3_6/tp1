/*************************************************************************
                           Ensemble  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface du module <Ensemble> (fichier Ensemble.h) -------------------
#if ! defined (ENSEMBLE_H)
#define ENSEMBLE_H

class Ensemble
{
/////////////////////////////////////////////////////////////////  INCLUDE
//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes
	static const int CARD_MAX=5;
	
//------------------------------------------------------------------ Types
	typedef enum { NON_INCLUSION , INCLUSION_LARGE , INCLUSION_STRICTE } crduEstInclus;
	typedef enum { DEJA_PRESENT, PLEIN , AJOUTE } crduAjouter;
		
//////////////////////////////////////////////////////////////////  PUBLIC
	public : 
//Fonctions publiques
// type Nom ( liste de paramètres );
// Mode d'emploi :
//
// Contrat :
		
		Ensemble (unsigned int cardMax = CARD_MAX); 
		Ensemble (int t[], unsigned int nbElements); 
		virtual ~Ensemble (); 
		void Afficher(void); 
		bool EstEgal(const Ensemble & unEnsemble) const;
		crduEstInclus EstInclus(const Ensemble & unEnsemble) const; 
		crduAjouter Ajouter (int aAjouter); 
		unsigned int Ajuster (int delta); 
		bool Retirer (int element); 
		unsigned int Retirer (const Ensemble & unEnsemble); 
		int Reunir (const Ensemble & unEnsemble); 
		unsigned int Intersection (const Ensemble & unEnsemble);

	
		
	protected : 
		bool doesExist(int valeurATester) const;
		void trier();
		bool estTrie();
		void swap (int & a, int & b);
		void sontEnCommun (const Ensemble & unEnsemble, bool tableauResultat[]);


		int cardinaliteMax; 
		int cardinaliteActuelle; 
		int * tab; //pointeur sur entiers
		
	
}; 
#endif // ENSEMBLE_H
