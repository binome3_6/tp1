/*************************************************************************
                           Ensemble  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
 *************************************************************************/

//---------- Réalisation de la classe <Ensemble> (fichier Ensemble.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>

//------------------------------------------------------ Include personnel
#include "Ensemble.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques


bool Ensemble::Retirer(int element) 
// Renvois "false" si l'élément n'a pas pu être retiré, "true" si il l'a été.
{
    bool reponse;
    if (doesExist(element))
    {
        int i = 0;
        while (tab[i] != element)
        {
            i++;
        }
#ifdef MAP
        cout << "on écrase l'élément : " << tab[i] << endl;
#endif
        for (i; i < cardinaliteActuelle - 1; i++)
        {
            tab[i] = tab[i + 1];
        }
        cardinaliteActuelle = cardinaliteActuelle - 1;
        reponse = true;
    }
    else
    {
#ifdef MAP
        cout << "L'élément " << element << " ne peut pas être retiré car il n'existe pas " << endl;
#endif
        reponse = false;
    }
    cardinaliteMax = cardinaliteActuelle;
    return reponse;
}


unsigned int Ensemble::Retirer(const Ensemble & unEnsemble) //A TESTER PAR MOI
{
    int cardMaxDebut = cardinaliteMax;
    int nbElementRetires = 0;
    int tableau[unEnsemble.cardinaliteActuelle];
    int cardinaliteActuelleASupprimer = unEnsemble.cardinaliteActuelle;

    //On rempli un tableau intermédiarie avec les valeurs à supprimer
    for (int i = 0; i < unEnsemble.cardinaliteActuelle; i++)
    {
        tableau[i] = unEnsemble.tab[i];
    }

    //On les supprime une par une
    for (int i = 0; i < cardinaliteActuelleASupprimer; i++)
    {

#ifdef MAP
        cout << "Cardinalite actuelle de l'ensemble à supprimer " << unEnsemble.cardinaliteActuelle << endl;
        cout << " passe N°" << i << " l'élément concerné est : " << unEnsemble.tab[i] << endl;
#endif

        bool test = this->Retirer(tableau[i]);
#ifdef MAP
        cout << "Réponse du test: " << test << endl;
#endif
        if (test)
        {
            nbElementRetires++;
        }
    }
    this->cardinaliteMax = cardMaxDebut;

    return nbElementRetires;

// Note : il aurait normalement été possible d'utiliser directement unEnsemble, cependant, des problèmes liés à des changements dans sa structure (bien qu'en const !) 
    // Nous ont forcé à changer notre méthode
}


unsigned int Ensemble::Intersection(const Ensemble & unEnsemble) //A TESTER PAR MOI
{
    int cardinaliteInitiale = cardinaliteActuelle;
    int valeursInitiales[cardinaliteActuelle];
    unsigned int elementsSupprimes = 0;

    //On rempli un tableau intermédiarie avec les valeurs à supprimer
    for (int i = 0; i < cardinaliteInitiale; i++)
    {
        valeursInitiales[i] = this->tab[i];
    }

    //On vérifie leur présence une par une
    for (int i = 0; i < cardinaliteInitiale; i++)
    {
        if (unEnsemble.doesExist(valeursInitiales[i]))
        {
            //on le garde
        }
        else
        {
            if (this->Retirer(valeursInitiales[i]))
            {
                elementsSupprimes++;
            }
        }
    }
    this->cardinaliteMax = cardinaliteActuelle;
    
    return elementsSupprimes;
}


void Ensemble::Afficher(void) {
    // Affichage de n et m
    cout << cardinaliteActuelle << "\r\n";
    cout << cardinaliteMax << "\r\n";

    trier();
    //Affichage du contenu de la liste
    cout << "{";
    for (int i = 0; i < cardinaliteActuelle; i++)
    {
        if (i != 0)
        {
            cout << ",";
        }
        cout << tab[i];
    }
    cout << "}\r\n";
} //----- Fin de Méthode


bool Ensemble::EstEgal(const Ensemble & unEnsemble) const
//Algorithme :
{

    if (cardinaliteActuelle != unEnsemble.cardinaliteActuelle)
    {
        return false;
    }

    //On créé un crible pour connaître les valeurs de this présentes dans unEnsemble.
    bool cribleValCommunes[cardinaliteActuelle];

    for (int i = 0; i < cardinaliteActuelle; i++)
    {
        cribleValCommunes[i] = false; //initialisation de la case
        for (int j = 0; j < unEnsemble.cardinaliteActuelle; j++)
        {
            if (tab[i] == unEnsemble.tab[i])
            {
                //Si la valeur de this est présente dans unEnsemble, on le crible
                cribleValCommunes[i] = true;
            }
        }

    }

    //Si il y a un seul faux, ils sont inégaux
    for (int i = 0; i < cardinaliteActuelle; i++)
    {
        if (cribleValCommunes[i] == false)
        {
            return false;
        }
    }

    //Si on est arrivé jusque là, ils sont de la même taille, et tous les éléments de this sont dans unEsnemble. Donc ils sont égaux.
    return true;
} // PEUT ETRE AMELIORE !


Ensemble::crduEstInclus Ensemble::EstInclus(const Ensemble & unEnsemble) const {

    crduEstInclus answer;

    // Si égaux = INCLUSION_LARGE
    if (this->EstEgal(unEnsemble))
    {
        answer = INCLUSION_LARGE; // CAS N°2
    }
    else if (this->cardinaliteActuelle > unEnsemble.cardinaliteActuelle)
    {
        // Si l'ensemble dont on veut tester l'inclusion est plus gros que l'autre. Directement : 
        answer = NON_INCLUSION; // CAS N°0
    }
    else
    {
        bool cribleValCommunes[cardinaliteActuelle];
        bool auMoinsUnFaux = false;

        // On crible la présence des valeurs dans un tableau booléan.
        for (int i = 0; i < cardinaliteActuelle; i++)
        {
            for (int j = 0; j < unEnsemble.cardinaliteActuelle; j++)
            {
                if (tab[i] == unEnsemble.tab[j])
                {
                    cribleValCommunes[i] = true;
                }
            }
        }

        //On réfléchi : si dans le crible, on a au moins 1 case marquée à "Faux", il n'y a pas inclusion
        for (int i = 0; i < cardinaliteActuelle; i++)
        {
            if (cribleValCommunes[i] == false)
            {
                auMoinsUnFaux = true;
            }
        }

        // Si un élément de this n'est pas dans un ensemble = NON_INCLUSION ( = auMoinsUnFaux = true)
        // Si tous les éléments de this sont dans unEnsemble, mais taille inégale = INCLUSION_STRICTE ( = auMoinsUnFaux = false)

        //On rends notre résultat : 
        answer = (auMoinsUnFaux ? NON_INCLUSION : INCLUSION_STRICTE);
    }

    return answer;
}


Ensemble::crduAjouter Ensemble::Ajouter(int aAjouter) {
    crduAjouter answer;
#ifdef MAP
    cout << "On tente l'ajout de : " << aAjouter << endl;
#endif
    if (doesExist(aAjouter))
    {
        answer = DEJA_PRESENT;
    }
    else if (cardinaliteActuelle < cardinaliteMax)
    {
        tab[cardinaliteActuelle] = aAjouter;
        cardinaliteActuelle++;
        answer = AJOUTE;
    }
    else
    {
        answer = PLEIN;
    }
    return answer;
}


unsigned int Ensemble::Ajuster(int delta) {
    unsigned int answer = cardinaliteMax;
#ifdef MAP
    cout << "L'ensemble a comme cardMax : " << cardinaliteMax << " delta fait " << delta << endl;
#endif
    if (delta > 0)
    {
        int *newTab = new int[cardinaliteMax + delta];
        for (int i = 0; i < cardinaliteActuelle; i++)
        {
            newTab[i] = tab[i];
        }
        tab = newTab;

        cardinaliteMax = cardinaliteMax + delta;
        answer = cardinaliteMax;
#ifdef MAP
        cout << "Après ajout (delta >0) L'ensemble a comme cardMax : " << cardinaliteMax << endl;
#endif
    }
    else if (delta < 0)
    {
        // Si il y a assez de place pour réduire
        if (-delta <= cardinaliteMax - cardinaliteActuelle) // si ça passe
        {
            cardinaliteMax = cardinaliteMax + delta; // car delta < 0
            answer = cardinaliteMax;
#ifdef MAP
            cout << "Après réduction (delta<0) l'ensemble a comme cardMax : " << cardinaliteMax << endl;
#endif
        }
        else // si il reste de la place, mais qu'on veut en supprimer trop, on reset le max à actuel
        {
            cardinaliteMax = cardinaliteActuelle;
            answer = cardinaliteMax;
        }
    }
#ifdef MAP
    cout << "On retourne finalement : " << cardinaliteMax << endl;
#endif
    return answer;
}


int Ensemble::Reunir(const Ensemble & unEnsemble) {
    //Rajouter tous les éléments de unEnsemble à this, qui ne sont pas dans déjà dans this.

    int answer = 0;
    // On construit un tableau des éléments à rajouter
    int aRajouter[unEnsemble.cardinaliteActuelle];
    int nbElementsARajouter = 0;

    // On vérifie si chaque élément du tableau en paramètre n'est pas déjà dans notre structure
    for (int i = 0; i < unEnsemble.cardinaliteActuelle; i++)
    {
        if (!doesExist(unEnsemble.tab[i]))
        {
            aRajouter[nbElementsARajouter] = unEnsemble.tab[i];
            nbElementsARajouter++;
        }
    }

#ifdef MAP
    cout << "On compte : " << cardinaliteMax - cardinaliteActuelle << " places et " << nbElementsARajouter << " Elements à rajouter" << endl;
#endif

    //On fait les tests pour se placer dans le bon cas.
    if (nbElementsARajouter == 0)
    {
        //Renvoi 0 si aucun élément rajouté.
        answer = nbElementsARajouter;
    }
    else if ((cardinaliteMax - cardinaliteActuelle) >= nbElementsARajouter) // il y a assez de place
    {
        //on rempli
        for (int i = 0; i < nbElementsARajouter; i++)
        {
            tab[cardinaliteActuelle] = aRajouter[i];
            cardinaliteActuelle++;
        }
#ifdef MAP
        cout << "Pas besoin de réajuster la liste " << endl;
#endif
        //Renvoi >0 si pas de réajustement.
        answer = nbElementsARajouter;
    }
    else //Il n'y a pas assez de place
    {
        Ajuster(nbElementsARajouter - (cardinaliteMax - cardinaliteActuelle));
#ifdef MAP
        cout << "On a ajusté la taille de la liste detla : " << nbElementsARajouter - (cardinaliteMax - cardinaliteActuelle) << endl;
#endif
        //on rempli
        for (int i = 0; i < nbElementsARajouter; i++)
        {
            tab[cardinaliteActuelle] = aRajouter[i];
            cardinaliteActuelle++;
        }

        //Renvoi <0 si réajustement pour faire tenir, au plus juste.
        answer = -nbElementsARajouter;
    }
#ifdef MAP
    cout << "Response de Reunir : " << answer << endl;
#endif
    return answer;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur

Ensemble::Ensemble(unsigned int cardMax)
// Algorithme :
{
#ifdef MAP
    cout << "Appel au constructeur de <Ensemble>" << endl;
#endif
    tab = new int[cardMax];
    cardinaliteMax = cardMax;
    cardinaliteActuelle = 0;
} //----- Fin de Ensemble (constructeur)

Ensemble::Ensemble(int t[], unsigned int nbElements)
// Algorithme :
{
#ifdef MAP
    cout << "Appel au constructeur de <Ensemble> avec paramètres" << endl;
#endif
    tab = new int[nbElements];
    cardinaliteActuelle = 0;
    cardinaliteMax = nbElements;

    for (unsigned int i = 0; i < nbElements; i++)
    {
        if (!doesExist(t[i])) // Si la valeur t[i] n'existe pas dans le tableau d'Ensemble alors ...
        {
            // On l'ajoute au tableau et on augmente le nombre de valeurs "valides" actuel.
            tab[cardinaliteActuelle] = t[i];
#ifdef MAP
            cout << "Ajout de : " << t[i] << " au tableau en place " << i << endl;
#endif
            cardinaliteActuelle++;
        }
    }

} //----- Fin de Ensemble (constructeur)

Ensemble::~Ensemble()
// Algorithme :
{
#ifdef MAP
    cout << "Appel au destructeur de <Ensemble>" << endl;
#endif
    delete [] tab;
} //----- Fin de ~Ensemble


//------------------------------------------------------------------ PRIVE


//----------------------------------------------------- Méthodes protégées

bool Ensemble::doesExist(int valeurATester) const {
    for (int i = 0; i < cardinaliteActuelle; i++)
    {
#ifdef MAP
        cout << "Test en cours : tableau = " << tab[i] << " valeur = " << valeurATester << endl;
#endif
        if (tab[i] == valeurATester)
        {
#ifdef MAP
            cout << valeurATester << " existe déjà en place " << i << endl;
#endif
            return true;
            // Return dans une boucle volontaire pour éviter de tester la fin du tableau si la valeur est déjà trouvée. Gain en performance.
        }
    }
    return false;
}

void Ensemble::trier() {
    while (!estTrie())
    {
        for (int i = 0; i < cardinaliteActuelle - 1; i++)
        {
            if (tab[i] > tab[i + 1])
            {
                swap(tab[i], tab[i + 1]);
            }
        }
    }
}

bool Ensemble::estTrie() {
    //Si une case i est de valeur supérieur à une case i+1 le tableau n'est pas trié.
    for (int i = 0; i < cardinaliteActuelle - 1; i++)
    {
        if (tab[i] > tab[i + 1])
        {
            return false;
        }
    }
    return true;
}

void Ensemble::swap(int & a, int & b) {
    int c = a;
    a = b;
    b = c;
}

void Ensemble::sontEnCommun(const Ensemble & unEnsemble, bool tableauResultat[]) {
    for (int i = 0; i < cardinaliteActuelle; i++)
    {
        for (int j = 0; j < cardinaliteActuelle; j++)
        {
            if (tab[i] == unEnsemble.tab[j])
            {
                tableauResultat[i] = true;
            }
        }
    }

}