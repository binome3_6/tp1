
/*************************************************************************
                           TEnsemble  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation du module <TEnsemble> (fichier TEnsemble.cpp) ---------------

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;
//------------------------------------------------------ Include personnel
#include "Ensemble.h"
//////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées
//static type nom ( liste de paramètres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

static void test1()
{
	//Tester constructeur par défaut & paramètres + destructeur
	//sans paramètre --> fournit cardinaliteMax=5; cardinaliteActuelle=0
	//paramètre --> cardinaliteMax=paramètre; cardinaliteActuelle=0
	//paramètre nul -- > cardinaliteActuelle=cardinaliteMax=0
	//destructeur explicite testé
	Ensemble e1(3); 
	Ensemble e2; 
	Ensemble e3(0); 
	e1.Afficher(); 
	e2.Afficher(); 
	e3.Afficher(); 
	
}

static void test2()
{
	//Tester constructeur avec parametres un tableau d'entiers + destructeur
	//test avec 1 element dans parametre & ensemble OK
	unsigned int nb = 1; 
	int tab1 [1] = {2};
	Ensemble e1(tab1, nb); 
	e1.Afficher(); 

	//test nbElements plus petit que la dimension du tableau de parametres
	//cardMax = 2; cardActuelle = 2 OK
	nb = 2; 
	int tab2 [3] = {0, 2, 3}; 
	Ensemble e2(tab2, nb); 
	e2.Afficher(); 
	
	//test avec 1 element dans ensemble, plusieurs elements en parametre OK
	nb = 3; 
	int tab3 [3] = {4, 4, 4};
	Ensemble e3(tab3, nb); 
	e3.Afficher();
	
	//test avec 11 elements dans parametre & 5 elements dans ensemble
	//test aussi nbElements different de la dimension du tableau de parametres OK
	nb = 11; 
	int tab4 [11] = {5, 6, 7, 5, 6, 7, 7, 0, 0, 1, 1}; 
	Ensemble e4(tab4, nb); 
	e4.Afficher();
	
	//test ensemble vide OK
	nb = 0; 
	int tab5 [0]; 
	Ensemble e5(tab5, nb); 
	e5.Afficher(); 
	
	//test avec parametre donné = dimension tableau; ensemble composé du nbElements à trier par ordre croissants OK
	nb = 3; 
	int tab6 [3] = {2, 1, 3};
	Ensemble e6(tab6, nb); 
	e6.Afficher();
}

static void test3()
{
	//Tester égalité entre deux ensembles
	//deux tableaux identiques --> ensembles identiques --> T
	unsigned int nb = 5; 
	int tab1 [5] = {1, 2, 5, 6, 7};
	int tab2 [5] = {1, 2, 5, 6, 7};
	Ensemble e1(tab1, nb); 
	Ensemble e2(tab2, nb); 
	e1.Afficher();
	e2.Afficher();
	cout << e1.EstEgal(e2) << endl; 
	//bool estEgal (const Ensemble & unEnsemble) const; 
	
	//deux ensembles identiques mais deux tableaux non triés --> T
	int tab3 [5]  = {1, 5, 2, 7, 6};
	int tab4 [5]  = {1, 6, 5, 2, 7};
	Ensemble e3(tab3, nb); 
	Ensemble e4(tab4, nb);
	e3.Afficher();
	e4.Afficher();
	cout << e3.EstEgal(e4) << endl; 
	
	//deux ensembles identiques mais cardinalités max differentes --> T
	int tab5 [5]  = {1, 5, 2, 6, 6}; 
	int tab6 [5]  = {1, 6, 5, 2, 7};
	Ensemble e5(tab5, nb); //cardmax = 5
	nb = 4; 
	Ensemble e6(tab6, nb); //cardmax = 4
	e5.Afficher();
	e6.Afficher();
	cout << e5.EstEgal(e6) << endl; 
	
	//deux ensembles non identiques avec cardinalités max identiques --> F
	int tab7 [4] = {1, 3, 2, 7};
	int tab8 [4] = {1, 7, 5, 2};
	Ensemble e7(tab7, nb); 
	Ensemble e8(tab8, nb);
	e7.Afficher();
	e8.Afficher();
	cout << e7.EstEgal(e8) << endl; 
	
	//deux ensembles de card égales mais différents comme ensembles --> F
	int tab9 [4] = {1, 4, 5, 2};
	Ensemble e9(tab9, nb);
	cout << e8.EstEgal(e9) << endl; 
	
}

static void test4()
{
	//Tester égalité entre deux ensembles
	//deux tableaux identiques --> INCLUSION_LARGE = 1
	unsigned int nb = 5; 
	int tab1 [nb] = {1, 2, 5, 6, 7};
	int tab2 [nb] = {1, 2, 5, 6, 7};
	Ensemble e1(tab1, nb); 
	Ensemble e2(tab2, nb); 
	e1.Afficher();
	e2.Afficher();
	cout << e1.EstInclus(e2) << endl; 
	
	//Inclusion de l'un dans l'autre : INCLUSION_STRICTE = 2
	int tab3 [3] = {1, 2, 5};
	int tab4 [5] = {1, 2, 5, 6, 7};
	Ensemble e3(tab3, 3); 
	Ensemble e4(tab4, 5); 
	e3.Afficher();
	e4.Afficher();
	cout << e3.EstInclus(e4) << endl; 
	
	//Non inclusion de l'un dans l'autre (tailles égales) : NON_INCLUSION = 0
	int tab5 [5] = {1, 2, 5, 8, 9};
	int tab6 [5] = {1, 2, 5, 6, 7};
	Ensemble e5(tab5, 5); 
	Ensemble e6(tab6, 5); 
	e5.Afficher();
	e6.Afficher();
	cout << e5.EstInclus(e6) << endl; 
	
	//Non inclusion de l'un dans l'autre (tailles non-égales) : NON_INCLUSION = 0
	int tab7 [4] = {1, 2, 8, 9};
	int tab8 [5] = {1, 2, 5, 6, 7};
	Ensemble e7(tab7, 4); 
	Ensemble e8(tab8, 5); 
	e7.Afficher();
	e8.Afficher();
	cout << e7.EstInclus(e8) << endl;
	
	// INCLUSION LARGE = 2
	int tab9 [0] = {};
	int tab10 [5] = {1, 2, 5, 6, 7};
	Ensemble e9(tab9, 0); 
	Ensemble e10(tab10, 5); 
	e9.Afficher();
	e10.Afficher();
	cout << e9.EstInclus(e10) << endl;
	
	// NON_INCLUSION = 0
	int tab11 [4] = {1, 2, 8, 9};
	int tab12 [0] = {};
	Ensemble e11(tab11, 4); 
	Ensemble e12(tab12, 0); 
	e11.Afficher();
	e12.Afficher();
	cout << e11.EstInclus(e12) << endl;
	
	// Inclusion large = 1 (taille égale)
	int tab13 [0] = {};
	int tab14 [0] = {};
	Ensemble e13(tab13, 0); 
	Ensemble e14(tab14, 0); 
	e13.Afficher();
	e14.Afficher();
	cout << e13.EstInclus(e14) << endl;
	
	// NON_INCLUSION = 0
	int tab15 [4] = {1, 2, 8, 9};
	int tab16 [2] = {4, 9};
	Ensemble e15(tab15, 4); 
	Ensemble e16(tab16, 2); 
	e15.Afficher();
	e16.Afficher();
	cout << e15.EstInclus(e16) << endl;
	
	// Inclusion large = 1
	int tab17 [4] = {1, 2, 8, 9};
	int tab18 [10] = {1, 1, 2 ,2 ,2 ,8 ,9 ,9 ,9 ,9};
	Ensemble e17(tab17, 4); 
	Ensemble e18(tab18, 10); 
	e17.Afficher();
	e18.Afficher();
	cout << e17.EstInclus(e18) << endl;
}

static void test5()
{
	// pas de place = code 1  puis 0
	int tab1 [5] = {1, 2, 5, 6, 7};
	Ensemble e1(tab1, 5); 
	e1.Afficher();
	cout << e1.Ajouter(8)<< endl;
	e1.Afficher();
	cout << e1.Ajouter(7)<< endl;
	e1.Afficher();
	
	// de la place mais des doublons puis ok : 0 puis 2
	int tab2 [4] = {1, 2, 6, 6};
	Ensemble e2(tab2, 4); 
	e2.Afficher();
	cout << e2.Ajouter(6)<< endl;
	e2.Afficher();
	cout << e2.Ajouter(8)<< endl;
	e2.Afficher();
	
	// Code (Ajoute) 2 et (present) 0.
	int tab3 [2] = {1, 2};
	Ensemble e3(tab3, 5); 
	e3.Afficher();
	cout << e3.Ajouter(0)<< endl;
	e3.Afficher();
	cout << e3.Ajouter(1)<< endl;
	e3.Afficher();
	
}

static void test6()
{
	//Ne passe pas (5) puis ok (6)
	int tab1 [5] = {1, 2, 5, 6, 7};
	Ensemble e1(tab1, 5); 
	e1.Afficher();
	cout << e1.Ajuster(-1)<< endl;
	e1.Afficher();
	cout << e1.Ajuster(1)<< endl;
	e1.Afficher();
	
	//Rien (4) puis rallongement (4+9=13)
	int tab2 [4] = {1, 2, 6, 6};
	Ensemble e2(tab2, 4); 
	e2.Afficher();
	cout << e2.Ajuster(0)<< endl;
	e2.Afficher();
	cout << e2.Ajuster(9)<< endl;
	e2.Afficher();
	
	cout << e2.Ajouter(6)<< endl;
	e2.Afficher();
	cout << e2.Ajouter(8)<< endl;
	e2.Afficher();
}

static void test7()
{
	
}

static void test8()
{
	
}

static void test9()
{
	//Ne rien faire = 0
	int tab1 [5] = {1, 2, 5, 6, 7};
	int tab2 [4] = {1, 2, 6, 6};
	
	Ensemble e1(tab1, 5); 
	Ensemble e2(tab2, 4); 
	
	e1.Afficher();
	e2.Afficher();
	cout << " ICI " << endl;
	cout << e1.Reunir(e2)<< endl;
	e1.Afficher();
	e2.Afficher();
	
	//Rajouter 3 case = -3
	int tab3 [5] = {1, 2, 3, 4, 5};
	int tab4 [3] = {6, 7, 8};
	
	Ensemble e3(tab3, 5); 
	Ensemble e4(tab4, 3); 
	
	e3.Afficher();
	e4.Afficher();
	cout << " ICI " << endl;
	cout << e3.Reunir(e4)<< endl;
	e3.Afficher();
	e4.Afficher();
	
	//Juste ajouter 3 cases : +3
	int tab5 [6] = {1, 2, 5, 5, 5, 5};
	int tab6 [3] = {6, 7, 8};
	
	Ensemble e5(tab5, 6); 
	Ensemble e6(tab6, 3); 
	
	e5.Afficher();
	e6.Afficher();
	cout << " ICI " << endl;
	cout << e5.Reunir(e6)<< endl;
	e5.Afficher();
	e6.Afficher();
}

static void test10()
{
	
}
//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques

int main()
// Algorithme :
//
{	
	//test1(); 
	//test2(); 
	//test3(); 
	//test4(); // PAS BON ! Mais ne sait pas en quoi c'est pas bon
	test6(); // Semble bon, mais pas le .o de test pour envoyer
	//test7();
	//test8();
	//test9();
	//test10();
	
	return 0; 
	
} //----- fin de main

